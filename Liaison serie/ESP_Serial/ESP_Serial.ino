#include <SoftwareSerial.h>


#define rxPin D7
#define txPin D6

SoftwareSerial mySerial(rxPin, txPin);     // mySerial =  



//données reçues
String inputString = "";        //une String pour stocker les données reçues
boolean stringComplete = false; //true si inputString est complète (le dernier caractère est \n)

void setup()  {
  Serial.begin(115200) ;
  // definie les pin pour tx, rx:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  // Config la vitesse de transmission
  mySerial.begin(38400);
  
  mySerial.println("Liaison serie en marche ");


  //réservation mémoire pour les String
  inputString.reserve(50); //50 caractères

}

//Réception sur la liaison série
void SofwareSerialEvent()
{
  //inputString = "" ;
  // quand des données arrivent sur le port série logiciel
  while (mySerial.available())
  {
    // enregistrer LE caractère reçu
    char inChar = (char)mySerial.read();
   // Serial.println(inChar);                //Envoie caractere par caratere a la serie usb ( Test )
    // le concaténer dans inputString:
    inputString += inChar;
    // Si le caractère reçu est le caractère de fin de ligne (\r)
    // Active le FLAG stringComplete (la boucle principale le traitera)
    if (inChar == '\r')
    {
      stringComplete = true;
    }
  }
}

void loop() {
  
  //Traitement des données reçues sur la laison série (appel à chaque itération de la boucle)
  SofwareSerialEvent();

  //Traitement principal
  // Si la chaine de carcatère reçue sur la liaison objSoftSerial est complète
  // (dernier caractère \n reçu)
  
  if (stringComplete)
  {
    // affiche sur l'USB de la chaine de caractère reçue
    Serial.println(inputString);
     
    // affiche sur objSoftSerial la chaine de caractère reçue
    mySerial.print("inputString bien reçue = ");
    mySerial.println(inputString);
    // réinitialise la chaine de caractères
    inputString = "";
    stringComplete = false;
  }
  
  // affiche sur objSoftSerial 
  //  mySerial.println("En attente d'un message");
  //delay(20000);
 

}
