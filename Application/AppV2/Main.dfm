object Fprinc: TFprinc
  Left = 491
  Top = 204
  Width = 840
  Height = 408
  Caption = 'Fprinc'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object GBConf: TGroupBox
    Left = 24
    Top = 24
    Width = 425
    Height = 313
    Caption = 'GBConf'
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 48
      Width = 31
      Height = 13
      Caption = 'SSID :'
    end
    object Label2: TLabel
      Left = 24
      Top = 88
      Width = 31
      Height = 13
      Caption = 'WAP :'
    end
    object Label3: TLabel
      Left = 24
      Top = 128
      Width = 50
      Height = 13
      Caption = 'IP Broker :'
    end
    object Label4: TLabel
      Left = 24
      Top = 168
      Width = 25
      Height = 13
      Caption = 'Port :'
    end
    object BtnEnvoyer: TButton
      Left = 16
      Top = 232
      Width = 393
      Height = 57
      Caption = 'BtnEnvoyer'
      TabOrder = 0
      OnClick = BtnEnvoyerClick
    end
    object EdtSSID: TEdit
      Left = 96
      Top = 48
      Width = 313
      Height = 21
      TabOrder = 1
    end
    object EdtBroker: TEdit
      Left = 96
      Top = 128
      Width = 313
      Height = 21
      TabOrder = 2
    end
    object EdtPort: TEdit
      Left = 96
      Top = 160
      Width = 313
      Height = 21
      TabOrder = 3
    end
    object EdtWPA: TEdit
      Left = 96
      Top = 80
      Width = 313
      Height = 21
      TabOrder = 4
    end
  end
  object GroupBox2: TGroupBox
    Left = 536
    Top = 24
    Width = 265
    Height = 105
    Caption = 'GroupBox2'
    TabOrder = 1
    object CbBoxPort: TComboBox
      Left = 48
      Top = 32
      Width = 145
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = 'CbBoxPort'
      Items.Strings = (
        'COM1'
        'COM2'
        'COM3')
    end
    object BtnConfPort: TButton
      Left = 72
      Top = 64
      Width = 113
      Height = 25
      Caption = 'BtnConfPort'
      TabOrder = 1
      OnClick = BtnConfPortClick
    end
  end
  object BtnFermer: TButton
    Left = 568
    Top = 256
    Width = 201
    Height = 65
    Caption = 'BtnFermer'
    TabOrder = 2
    OnClick = BtnFermerClick
  end
end
