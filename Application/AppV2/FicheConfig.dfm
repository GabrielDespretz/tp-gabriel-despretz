�
 TFCONFIG 0E  TPF0TFConfigFConfigLeft� Top� BorderStylebsDialogCaptionConfiguration du port COMClientHeight� ClientWidthyColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrder	PixelsPerInch`
TextHeight 	TGroupBoxGpBoxConfigLeftTopWidthYHeightACaption)   Configuration actuelle du port série COMFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder  TLabelLblVitesseActuLeftTopWidth%HeightCaptionVitesse:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabelLblFormatActuLeftTop(Width#HeightCaptionFormat:Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont   	TGroupBoxGpBoxVitesseLeftTopXWidthyHeightiCaptionVitesseFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder 	TComboBoxCbBoxVitesseLeftTop WidthaHeightStylecsDropDownList
ItemHeightTabOrder OnChangeCbBoxVitesseChangeItems.Strings1103006001200480096001440019200384005600057600115200128000256000    	TGroupBoxGpBoxFormatLeft� TopXWidth� HeightiCaptionFormatFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder TLabel	LblNbBitsLeftTop WidthLHeightCaption   Bits de donnéesColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclGreenFont.Height�	Font.NameMS Sans Serif
Font.Style ParentColor
ParentFont  TLabelLblStopLeftTopPWidth7HeightCaptionBits de stopFont.CharsetDEFAULT_CHARSET
Font.ColorclGreenFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  TLabel	LblPariteLeftTop8WidthHeightCaption   ParitéFont.CharsetDEFAULT_CHARSET
Font.ColorclGreenFont.Height�	Font.NameMS Sans Serif
Font.Style 
ParentFont  	TComboBoxCbBoxNbBitsLeftxTopWidthIHeightStylecsDropDownList
ItemHeightTabOrder OnChangeCbBoxNbBitsChangeItems.Strings78   	TComboBox	CbBoxStopLeftxTopHWidthIHeightStylecsDropDownList
ItemHeightTabOrderOnChangeCbBoxStopChangeItems.Strings12   	TComboBoxCbBoxPariteLeftxTop0WidthIHeightStylecsDropDownList
ItemHeightTabOrderOnChangeCbBoxPariteChange   TButton	BtnFermerLeftTop� WidthKHeightCaption&FermerTabOrderOnClickBtnFermerClick  TButtonBtnAppliquerLeft� Top� WidthKHeightCaption
&AppliquerEnabledTabOrderOnClickBtnAppliquerClick  TButton
BtnAnnulerLeft(Top� WidthKHeightCaptionA&nnulerTabOrderOnClickBtnAnnulerClick   