//
//
//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Main.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFprinc *Fprinc;
//---------------------------------------------------------------------------
__fastcall TFprinc::TFprinc(TComponent* Owner)
        : TForm(Owner)
{
        ptrSerie = new SerieWin32; //Creation de l'objet "WinSerie32"
        CbBoxPort->ItemIndex =0 ; // port COM1 select par default
}
//---------------------------------------------------------------------------


void __fastcall TFprinc::BtnFermerClick(TObject *Sender)
{
        delete ptrSerie ;      // destruction de l'objet "WinSerie32"
        Close();
}
//---------------------------------------------------------------------------

void __fastcall TFprinc::BtnConfPortClick(TObject *Sender)
{
  int codErr = 0 ;
AnsiString message ;
codErr = ptrSerie->ChoisirPort(CbBoxPort->Text); //selection du port a partir du combo box
if(codErr == -1){                                //Si le port existe pas ou pas disponible
  message = "Le port " + CbBoxPort->Text + "n'est pas dispo";
  ShowMessage(message);
  }else{
    ptrSerie->ConfigurerPort();                  //ouverture de la fiche config
    BtnEnvoyer->Enabled = true;
    }
}
//---------------------------------------------------------------------------

void __fastcall TFprinc::BtnEnvoyerClick(TObject *Sender)
{
AnsiString message;
if(ptrSerie->EstActif() == true){                     //si le port est dispo
  if((EdtSSID->Text != "") && (EdtWPA->Text != "")&&(EdtBroker->Text != "")
  &&(EdtPort->Text != "")){  //si les zone edtSSID ET EdtWAP son pas vide
    ptrSerie->EcrireDonnees(EdtSSID->Text);           //envoie la chaine de carractere EdtSSID
    ptrSerie->EcrireDonnees(EdtWPA->Text);            //envoie la chaine de carractere EdtWAP
    }else{
      ShowMessage("Il manque des informations");
    }
  }else{
    message = "Le port " + CbBoxPort->Text + "n'est pas disponible";
    ShowMessage(message);                                              //sinon afficher message erreur
    }


}
//---------------------------------------------------------------------------


void __fastcall TFprinc::CbBoxPortChange(TObject *Sender)
{
  ptrSerie->ChoisirPort(CbBoxPort->Text);
}
//---------------------------------------------------------------------------


