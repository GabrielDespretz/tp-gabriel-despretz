//---------------------------------------------------------------------------

#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>

#include <ClasseSerieWin32.h>
//---------------------------------------------------------------------------
class TFprinc : public TForm
{
__published:	// Composants gérés par l'EDI
        TGroupBox *GBConf;
        TLabel *Label1;
        TLabel *Label2;
        TLabel *Label3;
        TLabel *Label4;
        TButton *BtnEnvoyer;
        TGroupBox *GroupBox2;
        TEdit *EdtSSID;
        TEdit *EdtWPA;
        TEdit *EdtBroker;
        TEdit *EdtPort;
        TComboBox *CbBoxPort;
        TButton *BtnConfPort;
        TButton *BtnFermer;
        void __fastcall BtnFermerClick(TObject *Sender);
        void __fastcall BtnConfPortClick(TObject *Sender);
        void __fastcall BtnEnvoyerClick(TObject *Sender);
        void __fastcall CbBoxPortChange(TObject *Sender);
private:	// Déclarations de l'utilisateur
        SerieWin32* ptrSerie;  // acc?s object SerieWin32
public:		// Déclarations de l'utilisateur
        __fastcall TFprinc(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TFprinc *Fprinc;
//---------------------------------------------------------------------------
#endif
