// Fichier "ClasseSerieWin32.h"
// Classe "SerieWin32": acc�s et configuration du port s�rie.
//
// DMFT le 13/09/2002                   La classe "SerieNt"
//      Mis � jour le 24/09/2002        correctif + maj classe "SerieNt"
//      Mis � jour le 21/10/2005        transformation de "SerieNt" en "SerieWin32"
//      maj le 09/11/2005               compatibilit� avec "SerieNtV2": ajout d'une
//                                      seconde forme pour "ChoisirPort()"
//      Mis � jour le 30/11/2005:       ajout des trois fonctions de transfert
//                                      en mode binaire + fonction de lecture
//                                      nombre octets dans buffer de r�ception.

//---------------------------------------------------------------------------
#ifndef ClasseSerieWin32H
#define ClasseSerieWin32H
//---------------------------------------------------------------------------
#include <Classes.hpp>                  // Pour DCB, HANDLE

//---------------------------------------------------------------------------

class SerieWin32 {
    private:
      DCB dcbAncien,                    // DCB pr�sent sur le poste avant modif.
          dcbCourant ;                  // DCB actif sur le port courant
      AnsiString nomPort ;              // Nom du port s�rie courant
      bool actif ;                      // true si port s�rie actif, false sinon
      bool configModifiee ;             // true si config. modifiee par l'utilisateur

    protected:
      HANDLE hCom ;                     // Handle d'acc�s au port courant

    public:
      SerieWin32() ;                    // constructeur
      ~SerieWin32() ;                   // destructeur

      int ChoisirPort(AnsiString) ;     // ouvre le port s�rie de nom pass� en argument
      int ChoisirPort(AnsiString, DCB *) ;     // ouvre le port s�rie de nom pass�
                                               // en argument, config. par le DCB re�u
                                               // en argument
      int LireNomPort(AnsiString &) ;   // renvoie le nom du port courant
      int ConfigurerPort(void) ;        // affiche la fiche de configuration
      bool EstActif(void) ;             // retourne la valeur de l'attribut "actif"
      int LireEtatBuffRecept(void) ;    // renvoi le nombre d'octets pr�sents dans
                                        // le buffer de r�ception

      // Transfert de de donn�es en mode ASCII:
      int EcrireDonnees(AnsiString) ;   // envoie une cha�ne par la liaison s�rie
      int EcrireDonnees(char) ;         // envoie un caract�re par la liaison s�rie
      int EcrireDonnees(int) ;          // envoie un entier par la liaison s�rie
      int EcrireDonnees(double) ;       // envoie un r�el par la liaison s�rie

      int LireDonnees(AnsiString &, int) ;  // lit N car. sur la liaison s�rie
      int LireDonnees(char&) ;              // lit 1 car. sur la liaison s�rie
      // lit N car. sur la liaison s�rie, arr�t sur r�ception car. sp�cifi�
      int LireDonnees(AnsiString &, int &, char) ;

      // Transfert de donn�es en mode binaire (binaire brut ou "raw")
      int EcrireDonnees(BYTE [], int) ;     // envoie un vecteur d'octets par la liaison s�rie
      int LireDonnees(BYTE [], int) ;       // lit N octets sur la liaison s�rie
      // lit N octets sur la liaison s�rie, arr�t sur r�ception octet sp�cifi�
      int LireDonnees(BYTE [], int &, BYTE) ;

} ;

#endif

//------------------- Fin du fichier "ClasseSerieWin32.h"  -------------------
