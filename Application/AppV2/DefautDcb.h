// Fichier "DefautDCB.h"        le 20/04/2002  modifi� le 02/05/2002
// DCB par d�faut: 9600 bauds, 8 bits, 1 stop, pas de parit�.
// Pas de protocole mat�riel, pas de protocole logiciel.
//

//---------------------------------------------------------------------------
#ifndef DEFAUT_DCB_H
#define DEFAUT_DCB_H

//---------------------------------------------------------------------------
// Configuration: 9600 bauds, 8 bits, 1 stop, pas de parit�.
// Pas de protocole mat�riel, pas de protocole logiciel.
//

DCB DefautDCB = {
    sizeof(DCB), // DCBlength;           // sizeof(DCB)
    CBR_9600,    // BaudRate;            // Baudrate at which running
    true,        // fBinary: 1;          // Binary Mode (skip EOF check)
    0,           // fParity: 1;          // Enable parity checking
    0,           // fOutxCtsFlow:1;      // CTS handshaking on output
    0,           // fOutxDsrFlow:1;      // DSR handshaking on output
    0,           // fDtrControl:2;       // DTR Flow control
    0,           // fDsrSensitivity:1;   // DSR Sensitivity
    0,           // fTXContinueOnXoff: 1; // Continue TX when Xoff sent
    0,           // fOutX: 1;            // Enable output X-ON/X-OFF
    0,           // fInX: 1;             // Enable input X-ON/X-OFF
    0,           // fErrorChar: 1;       // Enable Err Replacement
    0,           // fNull: 1;            // Enable Null stripping
    0,           // fRtsControl:2;       // Rts Flow control
    0,           // fAbortOnError:1;     // Abort all reads and writes on Error
    0,           // fDummy2:17;          // Reserved
    0,           // wReserved;           // Not currently used
    0,           // XonLim;              // Transmit X-ON threshold
    0,           // XoffLim;             // Transmit X-OFF threshold
    8,           // ByteSize;            // Number of bits/byte, 4-8
    NOPARITY,    // Parity;              // 0-4=None,Odd,Even,Mark,Space
    ONESTOPBIT,  // StopBits;            // 0,1,2 = 1, 1.5, 2
    0x11,        // XonChar;             // Tx and Rx X-ON character
    0x13,        // XoffChar;            // Tx and Rx X-OFF character
    0,           // ErrorChar;           // Error replacement char
    0,           // EofChar;             // End of Input character
    0,           // EvtChar;             // Received Event character
    0            // wReserved1;          // Fill for now.
} ;


#endif
//------------------------- Fin de fichier "DefautDCB.h ---------------------