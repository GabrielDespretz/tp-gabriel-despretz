#include "FS.h"

void setup() {
  bool codErr = false ; 
  Dir dir ;

  Serial.begin(115200) ;

  Serial.println("ouverture FS") ;
  codErr = SPIFFS.begin();
  if(codErr == true){
    Serial.println("-------------------------------------------------------------------------");
    Serial.println("Liste des fichiers presents sur le disque");

    dir = SPIFFS.openDir("/");
    while(dir.next()) {
      Serial.print(String("\t") + dir.fileName());
      if(dir.fileSize()) {
        File f = dir.openFile("r");
        Serial.println(String("\t") + f.size());
        
        f.close();
      }
    }
    Serial.println("--------------------------------------------------------------------");
  }else {
    Serial.println("Erreur ouverture FS");
  }
}

void loop() {
  // put your main code here, to run repeatedly:

}
