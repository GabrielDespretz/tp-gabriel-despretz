#include <SoftwareSerial.h>
#include "FS.h"

#define rxPin D7
#define txPin D6

SoftwareSerial mySerial(rxPin, txPin);     // mySerial =  


//données reçues
String inputString = "";        //une String pour stocker les données reçues
boolean stringComplete = false; //true si inputString est complète (le dernier caractère est \n)
String EcritureDep;

void setup()  {
  Serial.begin(115200) ;
  // definie les pin pour tx, rx:
  pinMode(rxPin, INPUT);
  pinMode(txPin, OUTPUT);
  // Config la vitesse de transmission
  mySerial.begin(38400); 
  mySerial.println("Liaison serie en marche ");
  //réservation mémoire pour les String
  inputString.reserve(200); //200 caractères


  //Partie SPIFF
  
  bool codErr = false ; 
  
  
  Serial.println("ouverture FS") ;
  codErr = SPIFFS.begin();
  if(codErr == true){
    Serial.println("-------------------------------------------------------------------------");
    Serial.println("Liste des fichiers presents sur le disque");
    File f = SPIFFS.open("/config.conf", "r");   //Ouverture fichier pour le lire 
     Serial.println("Lecture du fichier en cours:"); //Affichage des données du fichier  
    EcritureDep = f.readString(); // on recupere le contenu entier du ficher 
    Serial.println(EcritureDep);   
    f.close();
    
    Serial.println("--------------------------------------------------------------------");
  }else {
    Serial.println("Erreur ouverture FS");
  }

}

//Réception sur la liaison série
void SofwareSerialEvent()
{
  //inputString = "" ;
  // quand des données arrivent sur le port série logiciel
  while (mySerial.available())
  {
    // enregistrer LE caractère reçu
    char inChar = (char)mySerial.read();
    Serial.println(inChar);                //Envoie caractere par caratere a la serie usb ( Test )
    // le concaténer dans inputString:
    inputString += inChar;
    // Si le caractère reçu est le caractère de fin de ligne (\r)
    // Active le FLAG stringComplete (la boucle principale le traitera)
    if (inChar == '\r')
    {
      stringComplete = true;
    }
  }
}

void loop() {
  
  //Traitement des données reçues sur la laison série (appel à chaque itération de la boucle)
  SofwareSerialEvent();

  //Traitement principal
  // Si la chaine de carcatère reçue sur la liaison objSoftSerial est complète
  // (dernier caractère \n reçu)
  
  if (stringComplete)
  {
    // affiche sur l'USB de la chaine de caractère reçue
    Serial.println(inputString);
     
    // affiche sur objSoftSerial la chaine de caractère reçue
    mySerial.print("inputString bien reçue = ");
    mySerial.println(inputString);
    // réinitialise la chaine de caractères
    inputString = "";
    stringComplete = false;
  }

//Partie SPIFF
// écrire dans fichier /config.conf
 File  f = SPIFFS.open("/config.conf", "w");
 f.println(EcritureDep);  // on réécrit l'ancien contenu sauvegardé au préalable 
 f.print("Ecriture nouvelle info dupuis sketch :"); // puis les nouvelles infos. 
 f.println(inputString);   
 f.close(); // on ferme le fichier une fois les enregistrements terminés

    // on ouvre de nouveau pour lecture
    f = SPIFFS.open("/config.conf", "r");
    EcritureDep = f.readString();
    //Affichage des données du fichier
    Serial.println("affichage de la nouvelle chaine de caractére :");
    Serial.println(EcritureDep);
    f.close();   //pause de 80 secondes , afin d'éviter de remplir la mémoire trop vite.Pensez à débrancher votre ESP ou d'uploader un nouveau sketch avant de saturer votre mémoire.
    delay(80000);
  


}
